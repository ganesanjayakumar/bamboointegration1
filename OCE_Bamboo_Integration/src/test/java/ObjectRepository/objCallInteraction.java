package ObjectRepository;

import org.openqa.selenium.By;

public class objCallInteraction
{
	public static By loginUserName = By.id("username");
	public static By loginPassword = By.id("password");
	public static By loginSubmit = By.id("Login");
	public static By passwordResetCancel = By.id("cancel-button");
	
	public static By attendeesTab = By.xpath("//div[@class='sidebar-item ']//div[@class='label' and contains(text(),'Attendees')]");
	
	public static By searchAllAttendees = By.xpath("//div[text()='Search All Attendees']");
	
	public static By crossClear = By.xpath("//span[@data-action='clear']");
	public static By typeSelection1 = By.xpath("(//div[@class='modal-attendee-name']/ancestor::div[2]/div/input)[1]");
	public static By onlyOneHCACanBeAdded = By.xpath("//div[@class='toasts-container']//div[contains(text(),'Only one HCA can be added to this call')]");

	public static By dateOlderThan30Days = By.xpath("//div[contains(text(),'Interaction cannot be created for a date older than 30 days')]");
	
}
