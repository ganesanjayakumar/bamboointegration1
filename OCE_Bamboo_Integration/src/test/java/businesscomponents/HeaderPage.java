package businesscomponents;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.mfa.framework.FrameworkException;
import com.mfa.framework.Status;
import ObjectRepository.objHeaderPage;
import supportlibraries.ConvenienceFunctions;
import supportlibraries.ReusableLibrary;
import supportlibraries.ScriptHelper;

public class HeaderPage extends ReusableLibrary
{
	private ConvenienceFunctions cf = new ConvenienceFunctions(scriptHelper);
	WebDriverWait wait = new WebDriverWait(driver.getWebDriver(), 30);

	public HeaderPage(ScriptHelper scriptHelper)
	{
		super(scriptHelper);
	}

	public void oceLogin() throws Exception
	{
		String strSalesForceUrl = properties.getProperty("OCEAppUrl");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		driver.get(strSalesForceUrl);

		if (cf.isElementVisible(objHeaderPage.loginUserName, "login - User Name"))
			report.updateTestLog("Verify OCE Login page is opened", "OCE Url: <b>" + strSalesForceUrl + "<b/>, Login page is opened", Status.PASS);
		else
			throw new FrameworkException("OCE Url: <b>" + strSalesForceUrl + "<b/>, Login page is not opened");

		String strUserName = dataTable.getData("Login", "SF_UserName");
		String strPassword = dataTable.getData("Login", "SF_Password");

		cf.setData(objHeaderPage.loginUserName, "User Name", strUserName);
		cf.setData(objHeaderPage.loginPassword, "Password", strPassword);
		cf.clickElement(objHeaderPage.loginSubmit, "Login Submit");

		if (cf.isElementVisible(objHeaderPage.passwordResetCancel, "PasswordResetCancel", 5))
		{
			report.updateTestLog("Verify password expired", "Login Password expired. Please update the password ", Status.WARNING);
			cf.clickElement(objHeaderPage.passwordResetCancel, "PasswordResetCancel");
		}

		String currentUrl = driver.getCurrentUrl();
		System.out.println(currentUrl);

		if (currentUrl.contains("one.app"))
		{
			String[] splitUrl = currentUrl.split("com");
			currentUrl = splitUrl[0] + "com/lightning/page/home";
			driver.get(currentUrl);
			driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		}
		else if (currentUrl.contains("my.salesforce"))
		{
			cf.clickElement(By.xpath("//div[@class='linkElements']/a[@class='switch-to-lightning']"), "Switch-to-lightning");
			driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);
		}

		driver.manage().window().fullscreen();
		cf.waitForSeconds(3);

	}

	public void logOut() throws Exception
	{
		cf.clickElement(By.xpath("//span[@class='photoContainer forceSocialPhoto']//span[@class='uiImage']"), "Avater Image");
		cf.waitForSeconds(3);

		report.updateTestLog("Verify Logout page", "Logout page", Status.SCREENSHOT);

		cf.clickElement(By.xpath("//div[@class='oneUserProfileCard']//a[@href='/secur/logout.jsp']"), "Logout");
		driver.manage().timeouts().pageLoadTimeout(80, TimeUnit.SECONDS);

		if (cf.isElementVisible(objHeaderPage.loginUserName, "login - User Name"))
			report.updateTestLog("Verify username is present", "Username is present", Status.PASS);
		else
			report.updateTestLog("Verify username is present", "Username is NOT present", Status.FAIL);

	}
}
