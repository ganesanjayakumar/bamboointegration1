package allocator;

import java.util.Arrays;

public class Analysis
{

	public static void factorial()
	{
		int i, fact = 1;
		int number = 5;//It is the number to calculate factorial    
		for (i = 1; i <= number; i++)
		{
			fact = fact * i;
		}
		System.out.println("Factorial of " + number + " is: " + fact);

	}

	public static void stringcheck()
	{
		int no = 8344;
		String calltime = String.valueOf(no);
		String[] dateTime = calltime.split("");
		calltime = dateTime[2] + dateTime[3];
		System.out.println(calltime);
	}

	public static void sortArray()
	{

		//Integer[] arr = {2, 5, 7, 3, 8};

		String[] arr = {"q", "a", "c"};

		//Arrays.sort(arr, Collections.reverseOrder());

		Arrays.sort(arr);

		System.out.println(Arrays.toString(arr));
	}
	
	public static void reverseString()
	{
		String a= "Jayakumar";
		
		/*	for(int i=a.length()-1;i>=0;i--)
			{
				System.out.print(a.charAt(i));
			}*/
		
		StringBuilder sb = new StringBuilder(a);
		
		int i = 100;
		
		String b = String.valueOf(i);
		
		StringBuilder sb1 = new StringBuilder(b);
		
		System.out.println(sb1.reverse());
		
		System.out.println(sb.reverse());
		
	}

	public static void main(String args[])
	{
		reverseString();
	}
}
